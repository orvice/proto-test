build:
	buf build

clean:
	rm -rf vendor/
	rm -rf gen/

test:
	buf build

.PHONY: gen
gen:
	buf generate --path=./comm
	cp go.mod gen/proto/go
